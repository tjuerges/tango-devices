'''
Run this Python3 script on your local machine like this:
python3 ConcatStringCommand.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
from tango.server import Device, attribute, command, run
from tango import DevString

class ConcatStringCommand(Device):
    @attribute
    def myAttribute(self):
        return "¡Hola mundo!"

    @command(dtype_in = DevString, dtype_out = DevString)
    def concatIt(self, inString: DevString = None) -> DevString:
        if inString is not None:
            return DevString(inString + "Foo")
        return DevString("Yo!")

if __name__ == "__main__":
    run((ConcatStringCommand,))
