'''
Run this Python3 script on your local machine like this:
python3 RandomData.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
from tango import DebugIt, DevState, EnsureOmniThread
from tango.server import Device, DevFailed, AttrWriteType, attribute, command, run
import numpy, threading, time

__all__ = ["RandomData", "main"]

class RandomData(Device):
    rnd1 = attribute(
        doc="This attribute is a random number which is updated every 1s",
        dtype='DevDouble',
        polling_period=100,
        period=1111,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=2222,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd100 = attribute(
        dtype=('DevDouble',),
        max_dim_x=1000,
        polling_period=100,
        period=1111,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=2222,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd1000 = attribute(
        dtype=(('DevDouble',),),
        max_dim_x=1000, max_dim_y=1000,
        polling_period=100,
        period=1111,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=2222,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    def init_device(self):
        """Initialises the attributes and properties of the RandomData."""
        Device.init_device(self)

        self.modify_attributes()

        self.rnd1.set_data_ready_event(True)
        self.set_change_event("rnd1", True, True)
        self.set_archive_event("rnd1", True, True)

        self.rnd100.set_data_ready_event(True)
        self.set_change_event("rnd100", True, True)
        self.set_archive_event("rnd100", True, True)

        self.rnd1000.set_data_ready_event(True)
        self.set_change_event("rnd1000", True, True)
        self.set_archive_event("rnd1000", True, True)

        self.set_state(DevState.ON)
        self.stop_updating = False
        self.thread = threading.Thread(target = self.modify_attributes_thread)
        self.thread.start()

    def modify_attributes(self):
        self._rnd1 = numpy.random.random()
        self._rnd100 = numpy.random.rand(1000)
        self._rnd1000 = numpy.random.rand(1000, 1000)

    def modify_attributes_thread(self):
        with EnsureOmniThread():
            while self.stop_updating is False:
                self.modify_attributes()
                time.sleep(1.0)

    def delete_device(self):
        self.stop_updating = True
        self.set_state(DevState.OFF)

    def read_rnd1(self):
        return self._rnd1

    def read_rnd100(self):
        return self._rnd100

    def read_rnd1000(self):
        return self._rnd1000


def main(args = None, **kwargs):
    return run((RandomData,), **kwargs)

if __name__ == '__main__':
    main()
