'''
Run this Python3 script on your local machine like this:
python3 AsyncioDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
import asyncio
from tango import DevState, GreenMode
from tango.server import Device, command, attribute
import numpy
from datetime import datetime

def now():
    return datetime.utcnow().strftime('%F %T.%f')[:-3]

class AsyncioDevice(Device):
    """Demo Tango Device Server using asyncio green mode"""
    green_mode = GreenMode.Asyncio

    async def init_device(self):
        await super().init_device()
        self.set_state(DevState.ON)

    @command
    async def long_running_command(self):
        self.set_state(DevState.OPEN)
        await asyncio.sleep(2)
        self.set_state(DevState.CLOSE)

    @command
    async def background_task_command(self):
        loop = asyncio.get_event_loop()
        future = loop.create_task(self.coroutine_target())

    async def coroutine_target(self):
        self.set_state(DevState.INSERT)
        print(f'{now()} State is now {self.get_state()}. Will wait for 15s before changing the state...')
        await asyncio.sleep(15)
        self.set_state(DevState.EXTRACT)
        print(f'{now()} Waited for 15s, state is now {self.get_state()}.')

    @attribute
    async def test_attribute(self):
        print(f'{now()} Wait for 2s before returning the attribute value...')
        await asyncio.sleep(2)
        print(f'{now()} Waiting for 2s done. Will now return the attribute value.')
        return numpy.random.random()


if __name__ == '__main__':
    AsyncioDevice.run_server(green_mode = GreenMode.Asyncio)
