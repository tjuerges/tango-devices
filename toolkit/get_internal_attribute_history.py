#! /usr/bin/env python3

from tango import DeviceProxy
from numpy import array, transpose

def get_internal_attribute_history(device_proxy: DeviceProxy, attribute_name: str, history_depth: int = 10):
    try:
        history = array(device_proxy.attribute_history(attr_name = attribute_name, depth = history_depth))
    except Exception as e:
        raise ValueError(f'Cannot access the internal history of the attribute {device_proxy.name()}/{attribute_name}. Exception: {e}') from e

    history_values = array([entry.value for entry in history])
    values = history_values.transpose()
    return (values, history)
