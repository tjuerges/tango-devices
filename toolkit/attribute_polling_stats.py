import numpy
import tango

def attribute_polling_stats(dp: tango._tango.DeviceProxy = None, iterations: int = 10, polling_time: float = 1.0, quiet = False):
    if dp is not None:
        print(f"Will sample the device server's polling time {iterations} times with a pause of {polling_time}s between each sampling.")
    else:
        print("A DeviceProxy object is needed!")
        return
    import numpy
    from time import sleep
    polling_durations = []
    polling_delays = []
    value = numpy.double(0)
    iterations_left = iterations
    while iterations_left > 0:
        iterations_left -= 1
        string = dp.polling_status()[0].split('\n')
        polling_duration = numpy.double(string[3].split('=')[-1].strip()) / 1e3
        polling_delay = numpy.double(string[5].split('=')[-1].split(',')[0].strip()) / 1e3
        polling_durations.append(polling_duration)
        polling_delays.append(polling_delay)
        if not quiet:
            print(f"Iteration #{iterations - iterations_left}, {iterations_left} iterations left, polling duration = {polling_duration}s, polling delay = {polling_delay}s.")
        sleep(polling_time)
    durations = numpy.array(polling_durations)
    delays = numpy.array(polling_delays)
    def compute_and_print(result):
        min = numpy.min(result)
        max = numpy.max(result)
        median = numpy.median(result)
        mean = numpy.mean(result)
        std = numpy.std(result)
        print(f"\tmin = {min}[s]\n\tmax = {max}[s]\n\tmedian = {median}[s]\n\tmean = {mean}[s]\n\tstddev = {std}[s]")
    print(f"\n\titerations = {iterations}\n\n\tPolling duration")
    compute_and_print(durations)
    print("\n\tPolling delay")
    compute_and_print(delays)
    return (durations, delays)
