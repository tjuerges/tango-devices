'''
Run this Python3 script on your local machine like this:
python3 segfault_DeviceTestContext.py
'''
from tango import (
    EventType,
    )
from tango.server import (
    Device
    )
from tango.test_context import DeviceTestContext
from tango.utils import (
    EventCallback,
    )
from time import (
    sleep,
    )

class Device1(Device):
    pass

with DeviceTestContext(Device1, process=False) as proxy:
    try:
        cb = EventCallback()
        id = proxy.subscribe_event("state", EventType.ATTR_CONF_EVENT, cb)
        sleep_time = 15  # sleep >~11 ==> SIGSEGV ; sleep<10 ==> exit OK
        for i in range(sleep_time):
            sleep(1)
            print(i+1, end=', ')
        print("\nbefore unsubscribe")
        proxy.unsubscribe_event(id)  # <-- SEGFAULT here
        print("after unsubscribe")
    except Exception as e:
        print(f'{e}')
