'''
Run this Python3 script on your local machine like this:
python3 DummyDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
import tango
import tango.server

class DummyDevice(tango.server.Device):
    def init_device(self):
        super(DummyDevice, self).init_device()
        self.__rw_attribute_not_polled__ = 0.0
        self.__rw_attribute_polled__ = 0.0
        self.set_state(tango.DevState.ON)

    @tango.server.command(dtype_out = tango.DevVarStringArray)
    def cmd(self) -> tango.DevVarStringArray:
        name = f'{self.get_name()}'
        state = f'{self.get_state()}'
        print(f'state = {state}\nget_name() = {name}')
        return (name, state)

    @tango.server.attribute
    def rw_attribute_not_polled(self) -> tango.DevDouble:
        return self.__rw_attribute_not_polled__

    @rw_attribute_not_polled.write
    def rw_attribute_not_polled(self, value: tango.DevDouble = None) -> tango.DevVoid:
        self.__rw_attribute_not_polled__ = value

    @tango.server.attribute(polling_period = 1000, rel_change = "1.0")
    def rw_attribute_polled(self) -> tango.DevDouble:
        return self.__rw_attribute_polled__

    @rw_attribute_polled.write
    def rw_attribute_polled(self, value: tango.DevDouble = None) -> tango.DevVoid:
        self.__rw_attribute_polled__ = value

if __name__ == '__main__':
    DummyDevice.run_server()
