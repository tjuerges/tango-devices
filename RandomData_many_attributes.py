'''
Run this Python3 script on your local machine like this:
python3 RandomData_many_attributes.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
from tango import DebugIt, DevState, EnsureOmniThread
from tango.server import Device, DevFailed, AttrWriteType, attribute, command, run
import numpy, threading, time

__all__ = ["RandomData", "main"]

class RandomData(Device):
    rnd1 = attribute(
        doc="This attribute is a random number which is updated every 1s",
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd2 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd3 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd4 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd5 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd6 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd7 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd8 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd9 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd10 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd11 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd12 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd13 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd14 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd15 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd16 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd17 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd18 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd19 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd20 = attribute(
        dtype='DevDouble',
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd100 = attribute(
        dtype=('DevDouble',),
        max_dim_x=1000,
        polling_period=100,
        period=1000,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=1000,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
        delta_t=1000,
        delta_val=0.1,
    )

    rnd1000 = attribute(
        dtype=(('DevDouble',),),
        max_dim_x=1000, max_dim_y=1000,
        polling_period=0,
        period=0,
        rel_change=0.1,
        abs_change=0.1,
        archive_period=0,
        archive_rel_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    def init_device(self):
        """Initialises the attributes and properties of the RandomData."""
        Device.init_device(self)

        self.modify_attributes()

        self.rnd1.set_data_ready_event(True)
        self.set_change_event("rnd1", True, True)
        self.set_archive_event("rnd1", True, True)

        self.rnd2.set_data_ready_event(True)
        self.set_change_event("rnd2", True, True)
        self.set_archive_event("rnd2", True, True)

        self.rnd3.set_data_ready_event(True)
        self.set_change_event("rnd3", True, True)
        self.set_archive_event("rnd3", True, True)

        self.rnd4.set_data_ready_event(True)
        self.set_change_event("rnd4", True, True)
        self.set_archive_event("rnd4", True, True)

        self.rnd5.set_data_ready_event(True)
        self.set_change_event("rnd5", True, True)
        self.set_archive_event("rnd5", True, True)

        self.rnd6.set_data_ready_event(True)
        self.set_change_event("rnd6", True, True)
        self.set_archive_event("rnd6", True, True)

        self.rnd7.set_data_ready_event(True)
        self.set_change_event("rnd7", True, True)
        self.set_archive_event("rnd7", True, True)

        self.rnd8.set_data_ready_event(True)
        self.set_change_event("rnd8", True, True)
        self.set_archive_event("rnd8", True, True)

        self.rnd9.set_data_ready_event(True)
        self.set_change_event("rnd9", True, True)
        self.set_archive_event("rnd9", True, True)

        self.rnd10.set_data_ready_event(True)
        self.set_change_event("rnd10", True, True)
        self.set_archive_event("rnd10", True, True)

        self.rnd11.set_data_ready_event(True)
        self.set_change_event("rnd11", True, True)
        self.set_archive_event("rnd11", True, True)

        self.rnd12.set_data_ready_event(True)
        self.set_change_event("rnd12", True, True)
        self.set_archive_event("rnd12", True, True)

        self.rnd13.set_data_ready_event(True)
        self.set_change_event("rnd13", True, True)
        self.set_archive_event("rnd13", True, True)

        self.rnd14.set_data_ready_event(True)
        self.set_change_event("rnd14", True, True)
        self.set_archive_event("rnd14", True, True)

        self.rnd15.set_data_ready_event(True)
        self.set_change_event("rnd15", True, True)
        self.set_archive_event("rnd15", True, True)

        self.rnd16.set_data_ready_event(True)
        self.set_change_event("rnd16", True, True)
        self.set_archive_event("rnd16", True, True)

        self.rnd17.set_data_ready_event(True)
        self.set_change_event("rnd17", True, True)
        self.set_archive_event("rnd17", True, True)

        self.rnd18.set_data_ready_event(True)
        self.set_change_event("rnd18", True, True)
        self.set_archive_event("rnd18", True, True)

        self.rnd19.set_data_ready_event(True)
        self.set_change_event("rnd19", True, True)
        self.set_archive_event("rnd19", True, True)

        self.rnd20.set_data_ready_event(True)
        self.set_change_event("rnd20", True, True)
        self.set_archive_event("rnd20", True, True)

        self.rnd100.set_data_ready_event(True)
        self.set_change_event("rnd100", True, True)
        self.set_archive_event("rnd100", True, True)

        self.rnd1000.set_data_ready_event(True)
        self.set_change_event("rnd1000", True, True)
        self.set_archive_event("rnd1000", True, True)

        self.set_state(DevState.ON)
        self.stop_updating = False
        self.thread = threading.Thread(target = self.modify_attributes_thread)
        self.thread.start()

    def modify_attributes(self):
        self._rnd1 = numpy.random.random()
        self._rnd2 = numpy.random.random()
        self._rnd3 = numpy.random.random()
        self._rnd4 = numpy.random.random()
        self._rnd5 = numpy.random.random()
        self._rnd6 = numpy.random.random()
        self._rnd7 = numpy.random.random()
        self._rnd8 = numpy.random.random()
        self._rnd9 = numpy.random.random()
        self._rnd10 = numpy.random.random()
        self._rnd11 = numpy.random.random()
        self._rnd12 = numpy.random.random()
        self._rnd13 = numpy.random.random()
        self._rnd14 = numpy.random.random()
        self._rnd15 = numpy.random.random()
        self._rnd16 = numpy.random.random()
        self._rnd17 = numpy.random.random()
        self._rnd18 = numpy.random.random()
        self._rnd19 = numpy.random.random()
        self._rnd20 = numpy.random.random()
        self._rnd100 = numpy.random.rand(1000)
        self._rnd1000 = numpy.random.rand(1000, 1000)

    def modify_attributes_thread(self):
        with EnsureOmniThread():
            while self.stop_updating is False:
                self.modify_attributes()
                time.sleep(1.0)

    def delete_device(self):
        self.stop_updating = True
        self.set_state(DevState.OFF)

    def read_rnd1(self):
        return self._rnd1

    def read_rnd2(self):
        return self._rnd2

    def read_rnd3(self):
        return self._rnd3

    def read_rnd4(self):
        return self._rnd4

    def read_rnd5(self):
        return self._rnd5

    def read_rnd6(self):
        return self._rnd6

    def read_rnd7(self):
        return self._rnd7

    def read_rnd8(self):
        return self._rnd8

    def read_rnd9(self):
        return self._rnd9

    def read_rnd10(self):
        return self._rnd10

    def read_rnd11(self):
        return self._rnd11

    def read_rnd12(self):
        return self._rnd12

    def read_rnd13(self):
        return self._rnd13

    def read_rnd14(self):
        return self._rnd14

    def read_rnd15(self):
        return self._rnd15

    def read_rnd16(self):
        return self._rnd16

    def read_rnd17(self):
        return self._rnd17

    def read_rnd18(self):
        return self._rnd18

    def read_rnd19(self):
        return self._rnd19

    def read_rnd20(self):
        return self._rnd20

    def read_rnd100(self):
        return self._rnd100

    def read_rnd1000(self):
        return self._rnd1000


def main(args = None, **kwargs):
    return run((RandomData,), **kwargs)

if __name__ == '__main__':
    main()
