"""Proof of concept Tango Device for the MID-ITF's Pi0.
This Device uses the asyncio green mode."""

import Adafruit_DHT as DHT
from asyncio import (
    get_event_loop,
    sleep
)
from datetime import datetime
from logging import (
    error,
    getLogger
)
import piplates.RELAYplate as RELAY
from threading import Thread
from tango import (
    DebugIt,
    DevDouble,
    DevFailed,
    DevState,
    DevUChar,
    DevVoid,
    ErrorIt,
    Except,
    GreenMode,
    log4tango
)
from tango.server import (
    attribute,
    command,
    Device
)

def now():
    return datetime.utcnow().strftime('%F %T.%f')[:-3]
class MidItfPi0Device(Device):
    green_mode = GreenMode.Asyncio
    DHT_SENSOR = DHT.DHT22
    DHT_PIN = 4
    RELAY_ADDRESS = 0
    RELAY_TARGETS = {
        'Correlated_Noise_Source': 1,
        'H_Channel': 2,
        'V_Channel': 3,
        'Uncorrelated_Noise_Sources': 4,
        'Band1': 5,
        'Band2': 6,
        'Band3': 7
    }
    RELAY_TARGETS_REVERSED = dict(map(reversed, RELAY_TARGETS.items()))

    async def switch_relay(self, relay_pin: str, onoff: bool) -> None:
        if onoff is True:
            RELAY.relayON(self.RELAY_ADDRESS, self.RELAY_TARGETS[relay_pin])
        else:
            RELAY.relayOFF(self.RELAY_ADDRESS, self.RELAY_TARGETS[relay_pin])

    async def read_all_relays(self) -> None:
        self.all_states = RELAY.relaySTATE(self.RELAY_ADDRESS)
        for relay, pin in self.RELAY_TARGETS.items():
            self.relays[relay] = (self.all_states >> (pin - 1)) & 1 == 1

    async def init_device(self):
        await super().init_device()

        self.isStopped = False

        self.logger = getLogger(__name__)

        self.sensors = {
            'humidity': 0.0,
            'temperature': 0.0
        }
        try:
            await self.read_humidity_temperature_from_hw()
        except DevFailed as ex:
            print(f"{ex}")

        self.relays = {
            'Correlated_Noise_Source': False,
            'H_Channel': False,
            'V_Channel': False,
            'Uncorrelated_Noise_Sources': False,
            'Band1': False,
            'Band2': False,
            'Band3': False
        }

        self.all_states = 0
        await self.read_all_relays()

        self.set_change_event('temperature', True, True)
        self.set_change_event('humidity', True, True)
        self.set_archive_event('temperature', True, True)
        self.set_archive_event('humidity', True, True)

        self.loop = get_event_loop()
        self.future = self.loop.create_task(self.read_humidity_temperature_from_hw())

        self.set_state(DevState.ON)

    async def delete_device(self):
        self.isStopped = True
        await sleep(5.0)

    async def read_humidity_temperature_from_hw(self) -> None:
        """
        Read humidity and temperature from the Raspberry's sensors.
        Store them in the sensors dictionary for random access.
        This command is automatically executed in the Device Server's polling
        loop.
        """
        while self.isStopped is False:
            humidity, temperature = DHT.read(self.DHT_SENSOR, self.DHT_PIN)
            if humidity is not None and temperature is not None:
                self.sensors['humidity'] = humidity
                self.sensors['temperature'] = temperature
            else:
                # Except.throw_exception("Sensor framework returned (None, None) as values.", "It was not possible to read temperature and/or humidity values from the sensors. The reason is likely a timing problem in the sensor framework and cannot be fixed at this level.", "read_humidity_temperature_from_hw")
                error(f"{now()} It was not possible to read temperature and/or humidity values from the sensors. The reason is likely a timing problem in the sensor framework and cannot be fixed at the level of this DeviceServer and Device.")
            await sleep(5.0)

    @attribute(polling_period = 10000, archive_rel_change = 1.0, rel_change = 1.0)
    async def temperature(self) -> DevDouble:
        return self.sensors['temperature']

    @attribute(polling_period = 10000, archive_rel_change = 1.0, rel_change = 1.0)
    async def humidity(self) -> DevDouble:
        return self.sensors['humidity']

    @attribute(dtype = DevUChar)
    async def All_Relay_States(self) -> DevUChar:
        await self.read_all_relays()
        return self.all_states

    @attribute(dtype = bool)
    async def Correlated_Noise_Source(self) -> bool:
        await self.read_all_relays()
        return self.relays['Correlated_Noise_Source']
    @Correlated_Noise_Source.write
    async def Correlated_Noise_Source(self, value: bool) -> DevVoid:
        await self.switch_relay('Correlated_Noise_Source', value)

    @attribute(dtype = bool)
    async def H_Channel(self) -> bool:
        await self.read_all_relays()
        return self.relays['H_Channel']
    @H_Channel.write
    async def H_Channel(self, value: bool) -> DevVoid:
        await self.switch_relay('H_Channel', value)

    @attribute(dtype = bool)
    async def V_Channel(self) -> bool:
        await self.read_all_relays()
        return self.relays['V_Channel']
    @V_Channel.write
    async def V_Channel(self, value: bool) -> DevVoid:
        await self.switch_relay('V_Channel', value)

    @attribute(dtype = bool)
    async def Uncorrelated_Noise_Sources(self) -> bool:
        await self.read_all_relays()
        return self.relays['Uncorrelated_Noise_Sources']
    @Uncorrelated_Noise_Sources.write
    async def Uncorrelated_Noise_Sources(self, value: bool) -> DevVoid:
        await self.switch_relay('Uncorrelated_Noise_Sources', value)

    @attribute(dtype = bool)
    async def Band1(self) -> bool:
        await self.read_all_relays()
        return self.relays['Band1']
    @Band1.write
    async def Band1(self, value: bool) -> DevVoid:
        await self.switch_relay('Band1', value)

    @attribute(dtype = bool)
    async def Band2(self) -> bool:
        await self.read_all_relays()
        return self.relays['Band2']
    @Band2.write
    async def Band2(self, value: bool) -> DevVoid:
        await self.switch_relay('Band2', value)

    @attribute(dtype = bool)
    async def Band3(self) -> bool:
        await self.read_all_relays()
        return self.relays['Band3']
    @Band3.write
    async def Band3(self, value: bool) -> DevVoid:
        await self.switch_relay('Band3', value)


if __name__ == '__main__':
    MidItfPi0Device.run_server(green_mode = GreenMode.Asyncio)
