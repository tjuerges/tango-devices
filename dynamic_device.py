'''
Run this Python3 script on your local machine like this:
python3 dynamic_device.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
from tango import Except, DevFailed, DevState, AttrWriteType, DebugIt, DeviceProxy, Util
from tango.server import Device, run, command, device_property, attribute
from tango import EventType

from functools import wraps

import time
from datetime import datetime


def log_exceptions():
    """ Decorator that logs all exceptions that the function raises. """
    def wrapper(func):
        @wraps(func)
        def inner(self, *args, **kwargs):
            try:
                return func(self, *args, **kwargs)
            except Exception as e:
                self.error_stream("An unhandled exception occured: {}".format(e))
                raise e
        return inner
    return wrapper


class Task(Device):
    time = attribute(dtype = float, access = AttrWriteType.READ, polling_period = 1000, period = 1000, rel_change = "1.0")
    date = attribute(dtype = str, access = AttrWriteType.READ)

    @DebugIt()
    def read_time(self):
        return time.time()

    @DebugIt()
    def read_date(self):
        return "{}".format(datetime.today())

    @DebugIt()
    def init_device(self):
        Device.init_device(self)
        self.device_proxy = None
        self.set_state(DevState.ON)
        self.info_stream("The Task has been started")


class Controller(Device):
    @DebugIt()
    def init_device(self):
        Device.init_device(self)
        self.device_proxy = None
        self.tango_util = Util.instance()
        self.myTangoDomain = self.get_name().split('/')[0]
        self.device_name = "{}/Task/12345".format(self.myTangoDomain)
        self.attribute_name = "{}/time".format(self.device_name)
        self.set_state(DevState.ON)
        self.info_stream("The Controller has been started.")


    @DebugIt()
    def delete_device(self):
        if self.get_state() != DevState.OFF:
            self.set_state(DevState.OFF)
            self.stop_task()
            self.info_stream("The Task device has been stopped and removed.")

    @DebugIt()
    def callback(self, event):
        self.info_stream("Received an event: {}".format(event))
        if not event.err:
            # Get the date attribute from the sending device.
            self.info_stream("The Task device's date attribute={}.".format(event.device.date))
        else:
            # Something is fishy with this event.
            self.warn_stream("The Task device {} sent an on-change event but the event reports an error.  It is advised to check the logs for any indication that something went wrong in that device.  Event data={} ".format(event, event.attr_name))

    @command()
    def start_task(self):
        self.debug_stream("device name = {}, attribute name = {}".format(self.device_name, self.attribute_name))

        # Create the Observation device instance in the Tango DB.
        # This will instantiate it and also call init_device.
        try:
            self.tango_util.create_device(Task.__name__, self.device_name)
        except DevFailed as ex:
            try:
                self.tango_util.delete_device(Observation.__name__, self.device_name)
            except:
                self.warn_stream("Cannot remove the Task device {} from the Tango DB.  Please make sure that you manually delete it.".format(self.device_name))
            error_string="Cannot start the Task device instance with device class name={} and device instance name={}.".format(Task.__name__, self.device_name)
            self.error_stream("{}, {}".format(error_string, ex))
            Except.re_throw_exception(ex, "DevFailed", error_string, __name__)

        try:
            # Instantiate a dynamic Tango Device "Task".
            self.device_proxy = DeviceProxy(self.device_name)

            # Subscribe to changes of the Task.time attribute.
            # This needs the device address.

            # Activate the polling for the observation_running_R MP.
            self.device_proxy.poll_attribute(self.attribute_name.split('/')[-1], 1000)
            # And now subscribe to on-change events.
            self.event_id = self.device_proxy.subscribe_event(self.attribute_name.split('/')[-1], EventType.PERIODIC_EVENT, self.callback)
        except DevFailed as ex:
            try:
                # Remove the device again.
                self.tango_util.delete_device(Task.__name__, self.device_name)
            except DevFailed as e:
                self.debug_stream("Something went wrong when the Task device was removed: {}".format(e))
                pass
            error_string="Cannot access the Task device instance with device class name={} and device instance name={}.".format(Task.__name__, self.device_name)
            self.error_stream("{}, {}".format(error_string, ex))
            Except.re_throw_exception(ex, "DevFailed", error_string, __name__)
        else:
            self.info_stream("Added and started Task {}.".format(self.device_name))

    @command()
    @DebugIt()
    def stop_task(self):
        # Check if the device has not terminated itself in the meanwhile.
        if self.device_proxy is not None:
            try:
                self.device_proxy.ping()
                # Unsubscribe from the subscribed event.
                self.device_proxy.unsubscribe_event(self.event_id)
            except DevFailed as ex:
                self.warn_stream("The Task device has unexpectedly already disappeared.  It is advised to check the logs up to 10s prior to this message to see what happened.")

        try:
            # Remove the device object from the Tango DB.
            self.tango_util.delete_device(Task.__name__, self.device_name)
        except DevFailed as e:
            self.warn_stream("Something went wrong when the device {} was removed from the Tango DB.  There is nothing that can be done about this here at this moment but you should check the Tango DB yourself.".format(self.device_name))



def main(args = None, **kwargs):
    return run((Controller, Task), verbose = True, args = args, **kwargs)


if __name__ == '__main__':
    main()
