'''
Run this Python3 script on your local machine like this:
python3 TestDeviceAlarm.py
'''
import logging
from tango.server import Device, attribute, command
from tango import DeviceAttribute, DevState, AttrQuality, CmdArgType
from tango.test_utils import DeviceTestContext

def test_bypass_alarm_state_for_out_of_range_attributes():
    class TestDevice(Device):
        _attr_value = 0

        def init_device(self):
            super(TestDevice, self).init_device()
            self.set_state(DevState.ON)

#         def dev_state(self):
#             """Override default Tango behaviour!
#             https://gitlab.com/tango-controls/cppTango/-/blob/9.4.0/cppapi/server/device.cpp#L1404
#             """
#             return self.get_state()

        @attribute(dtype=int, max_alarm=10)
        def attr(self):
            return self._attr_value

        @attr.write
        def attr(self, value):
            self._attr_value = value

        @command(dtype_in = CmdArgType.DevState)
        def modify_state(self, _state: CmdArgType.DevState):
            self.set_state(_state)

    with DeviceTestContext(TestDevice) as proxy:
        def modify(_value: int, _state: DevState = None) -> DeviceAttribute:
            print(f'\nsetting value = {_value}')
            proxy.attr = _value
            if _state is not None:
                print(f'setting state = {_state}')
                proxy.modify_state(_state)
            attr = proxy.read_attribute("attr")
            print(f'{attr}')
            value = attr.value
            quality = attr.quality
            state = proxy.state()
            print(f'\tattr.value = {value}\n\tattr.quality = {quality}\n\tdevice state = {state}\n')
            return quality, state

        def scenario_1():
            print('*** Scenario 1')
            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

            # Set the attribute value to alarm
            quality, state = modify(20)
            assert quality is AttrQuality.ATTR_ALARM
            try:
                assert state == DevState.ON # if we don't override dev_state we get ALARM here.
            except AssertionError as e:
                assert state == DevState.ALARM

            # Reset value to nominal
            quality, state = modify(5)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

        def scenario_2():
            print('*** Scenario 2')
            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

            # Set value to alarm
            quality, state = modify(20)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.ALARM

            # Set to alarm but different state
            quality, state = modify(20, DevState.MOVING)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.MOVING

            # Reset state
            quality, state = modify(20, DevState.ON)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.ALARM

            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

        def scenario_3():
            print('*** Scenario 3')
            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

            # Set to nominal value, different state
            quality, state = modify(5, DevState.MOVING)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.MOVING

            # Set to alarm value, keep state
            quality, state = modify(20)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.MOVING

            # Keep alarm value, set state nominal
            quality, state = modify(20, DevState.ON)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.ALARM

            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

        def scenario_4():
            print('*** Scenario 4')
            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

            # Set to nominal value, different state
            quality, state = modify(5, DevState.MOVING)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.MOVING

            # Set to alarm value, keep state
            quality, state = modify(20)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.MOVING

            # Keep alarm value, set state nominal
            quality, state = modify(20, DevState.ON)
            assert quality is AttrQuality.ATTR_ALARM
            assert state == DevState.ALARM

            # Reset to nominal
            quality, state = modify(5, DevState.ON)
            assert quality is AttrQuality.ATTR_VALID
            assert state == DevState.ON

        scenario_1()
        scenario_2()
        scenario_3()
        scenario_4()

def main(args = None, **kwargs):
    return test_bypass_alarm_state_for_out_of_range_attributes()

if __name__ == '__main__':
    main()
