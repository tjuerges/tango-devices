'''
Run this Python3 script on your local machine like this:
python3 SimpleAsyncDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
import asyncio
from tango import DevState, GreenMode, DevDouble, DevVoid
from tango.server import Device, command, attribute, run

import numpy

from time import sleep
from datetime import datetime

# I want to print neat ISO timestamps.
def now():
    return datetime.utcnow().strftime('%FT%T.%f')[:-3]

class SimpleAsyncDevice(Device):
    """This is a very simple Tango Device"""
    async def init_device(self):
        await super().init_device()
        self._sleepTime = 0.0
        self.set_state(DevState.ON)

    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    async def wait_cmd(self, sleepTime: DevDouble = None) -> DevVoid:
        print(f'{now()} The command will wait for {sleepTime}s before returning...')
        await asyncio.sleep(sleepTime)
        print(f'{now()} The command waited for {sleepTime}s.')

    @attribute
    async def sleepTime(self) -> None:
        return self._sleepTime

    @sleepTime.write
    async def sleepTime(self, sleepTime: DevDouble = None) -> DevVoid:
        print(f'{now()} Setting the sleep time.')
        self._sleepTime = sleepTime

    @attribute
    async def test_attribute(self) -> DevDouble:
        print(f'{now()} Wait for {self._sleepTime}s before returning the attribute value...')
        await asyncio.sleep(self._sleepTime)
        self._test_attribute = numpy.random.random()
        print(f'{now()} Waiting for {self._sleepTime}s done. Will now return the attribute value.')
        return self._test_attribute

if __name__ == '__main__':
    run((SimpleAsyncDevice, ), green_mode = GreenMode.Asyncio)
    print(f'{now()}')
