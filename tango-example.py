'''
Run this Python3 script on your local machine like this:
python3 tango-example.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
import time
import threading
import numpy
from tango import DevState
from tango.server import run
from tango.server import Device
from tango.server import attribute, command

class DeadLock(Device):
    noise = attribute(label="Noise", dtype=((int,),), max_dim_x=1024, max_dim_y=1024)

    class PushingThread(threading.Thread):
        def __init__(self, device):
            self._stop = False
            self._device = device
            super(DeadLock.PushingThread, self).__init__()
            self.timestamp = time.time()
        def run(self):
            while self._stop is False:
                self._device.push_change_event("noise", self._device.read_noise())
                self.timestamp=time.time()
#### No time spacing!

    def init_device(self):
        Device.init_device(self)

        self._lock = threading.Condition()
        self._stop = False

        self.set_change_event('noise', True, False)

        self._pushing_event_thread = self.PushingThread(self)
        self._pushing_event_thread.start()

    def read_noise(self):
        return numpy.random.random_integers(1000, size=(100, 100))

    @command
    def start(self):
        self.set_state(DevState.ON)

    @command
    def stop(self):
        with self._lock:
            self._pushing_event_thread._stop=True
            self._lock.notify()
        self._pushing_event_thread.join()
        self.set_state(DevState.OFF)

if __name__ == "__main__":
    DeadLock.run_server()
