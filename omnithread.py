'''
Run this Python3 script on your local machine like this:
python3 omnithread.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
import platform
dp = tango.DeviceProxy(f'tango://{platform.node()}:45678/foo/bar/1#dbase=no')
'''
from tango import (
	DevState,
	EnsureOmniThread
)
from tango.server import (
	Device,
	run
)
import threading, time

class Foo(Device):
    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)
        self.thread = threading.Thread(target = self.yo_thread)
        self.thread.start()

    def yo_thread(self):
        with EnsureOmniThread():
            while True:
                time.sleep(1.0)
                print('Yo')

def main(args = None, **kwargs):
    return run((Foo,), **kwargs)

if __name__ == '__main__':
    main()
